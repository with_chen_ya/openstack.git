#!/bin/bash
##数据库安装脚本
#!/bin/bash
##一键安装配置mariadb数据库
source openstack/info/install-info.sh
yum install mariadb mariadb-server MySQL-python expect ntpd -y
crudini --set /usr/lib/systemd/system/mariadb.service Service LimitNOFILE 10000
crudini --set /usr/lib/systemd/system/mariadb.service Service LimitNPROC 10000
echo -e 'default-storage-engine = innodb
innodb_file_per_table
collation-server = utf8_general_ci
init-connect = 'SET NAMES utf8'
character-set-server = utf8' >> /etc/my.cnf
systemctl daemon-reload
systemctl enable mariadb.service
if [ -z `systemctl restart mariadb.service` ];then
	echo -e "数据库服务启动成功"
else
	echo -e "数据库服务启动失败"
	exit 0
fi
echo -e ' --------初始化数据库------------'
expect -c "
spawn /usr/bin/mysql_secure_installation
expect \"Enter current password for root (enter for none):\"
send \"\r\"
expect \"Set root password?\"
send \"y\r\"
expect \"New password:\"
send \"$ALL_PASS\r\"
expect \"Re-enter new password:\"
send \"$ALL_PASS\r\"
expect \"Remove anonymous users?\"
send \"y\r\"
expect \"Disallow root login remotely?\"
send \"n\r\"
expect \"Remove test database and access to it?\"
send \"y\r\"
expect \"Reload privilege tables now?\"
send \"y\r\"
expect eof
"
mysql -uroot -p$ALL_PASS -e "GRANT ALL PRIVILEGES ON *.* TO 'root'@'%' IDENTIFIED BY '$ALL_PASS';"
for db in ksystone glance nova nova_api neutron;do
	mysql -uroot -p$ALL_PASS -e "create database IF NOT EXISTS $db;"
	mysql -uroot -p$ALL_PASS -e "GRANT ALL PRIVILEGES ON $db.* TO '$db'@'%' IDENTIFIED BY '$ALL_PASS';"
	mysql -uroot -p$ALL_PASS -e "GRANT ALL PRIVILEGES ON $db.* TO '$db'@'localhost' IDENTIFIED BY '$ALL_PASS';"
done
echo -e '------------------初始化数据库完成-------------'
systemctl restart mariadb.service
yum installl -y upgrade crudini python2-PyMySQL expect mongodb-server mongodb rabbitmq-server memcached python-memcached
echo -e '配置mongod'
sed -i -e '/bind_ip/d' -e 's/#smallfiles.*/smallfiles=true/g' /etc/mongod.conf 
systemctl enable mongod.service
systemctl restart mongod.service
# rabbitmq
systemctl enable rabbitmq-server.service
if [ -z `systemctl restart rabbitmq-server.service` ];then
	echo -e "数据库服务启动成功"
else
	echo -e "rabbitmq-server.service服务启动失败"
	exit 0
fi
rabbitmqctl add_user $RABBIT_USER $ALL_PASS
rabbitmqctl set_permissions $RABBIT_USER ".*" ".*" ".*"
# memcache
systemctl enable memcached.service
if [ -z `systemctl restart memcached.service` ];then
	echo -e "数据库服务启动成功"
else
	echo -e "rabbitmq-server.service服务启动失败"
	exit 0
fi
