#!/bin/bash
##决定本机安装所需要安装的脚本
na=`hostname`
if [ $na == controller ];then
	read -p "在此节点安装控制服务[y/n]" kz
	if [ $kz == y ];then
		echo -e '开始安装控制服务'
	else
		echo -e '开始安装计算服务'
	fi
else
	echo -e '请选择此节点的类型'
	echo -e '[1]控制节点(controller)'
	echo -e '[2]计算节点(compute)'
	read xz
	if [ $xz == 1 ];then
		echo -e '开始执行对应脚本'
	elif [ $xz == 2 ];then
		echo -e '开始执行对应脚本'
	else
		echo -e '选择错误'
		exit 0
	fi
fi
