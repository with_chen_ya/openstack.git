#!/bin/bash
##Centos7一键配置OpenStack
source openstack/info/install-info.sh
if [ -n $Successs ];then
	source openstack/info/install-info.sh
	echo -e "$HOST_IP controller" >> /etc/hosts
	echo -e "$HOST_IP_NODE compute" >> /etc/hosts
	##关闭selinux及防火墙,NetworkManager
	sed -i 's/SELINUX=.*/SELINUX=disabled/g' /etc/sysconfig/selinux
	systemctl stop firewalld NetworkManager.service
	yum remove -y firewalld NetworkManager.service
	setenforce 0
	yum install ntp  iptables-services  -y 
	systemctl enable iptables
	systemctl restart iptables
	iptables -F
	iptables -X
	iptables -X
	service iptables save
	if [[ `ip a |grep -w $HOST_IP ` != '' ]];then 
		hostnamectl set-hostname $HOST_NAME
		echo -e "已配置该主机名为$HOST_NAME"
	elif [[ `ip a |grep -w $HOST_IP_NODE ` != '' ]];then 
		echo -e "已配置该主机名为$HOST_NAME_NODE"
		hostnamectl set-hostname $HOST_NAME_NODE
	else
		echo -e '无法判断此节点类型，请输入该节点需要配置的主机名'
		read hname
		hostnamectl set-hostname $hname
		echo -e "已配置该主机名为$hname"
	fi
	sed -i -e 's/#UseDNS yes/UseDNS no/g' -e 's/GSSAPIAuthentication yes/GSSAPIAuthentication no/g' /etc/ssh/sshd_config
	echo -e '开始安装服务'
	yum upgrade -y
	yum install crudini -y
	yum -y install openstack-selinux python-openstackclient  -y
	echo  '请重启该系统后继续操作[重要]'
else
	echo -e '请先使用命令
			vi openstack/info/install-info.sh
			编辑相关信息再继续'
			exit 0
fi
