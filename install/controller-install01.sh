#!/bin/bash
echo 'controller'
echo -e "系统已经预配置,开始安装"
echo -e '开始安装数据库'
bash openstack/install/mariadb.sh
echo -e '开始安装Keystone组件'
sh openstack/install/keystone.sh
echo -e '开始安装glance镜像服务'
bash openstack/install/glance.sh
echo -e '开始安装Nova服务'
bash openstack/install/nova-controller.sh
echo -e '开始安装网络组件'
bash openstack/install/neutron-controller.sh
echo -e "请选择网络安类型"
echo -e "[1]GRE[默认]"
echo -e "[2]VLAN"
read xze
if [ $xze == 1 ];then
	sh openstack/install/neutron-controller-gre.sh
elif [ $xze == 2 ];then
	echo 'sh openstack/install/neutron-controller-gre.sh'
else
	sh openstack/install/neutron-controller-gre.sh
fi
echo -e '开始安装dashboard'
bash openstack/install/dashboard.sh
echo -e '完成'
echo -e '基础服务已经安装完成,是否安装扩展服务[y/n]'
read pp
if [ $pp == y ];then
	echo -e '待完善'
else
	echo -e '完成'
fi
echo -e '配置完成'