#!/bin/bash
##一键安装keystone组件及依赖服务
source openstack/info/install-info.sh
echo -e "-----------------安装openstack服务---------------"
yum install -y openstack-selinux python-openstackclient
yum install -y crudini  ntpd
yum install -y openstack-keystone httpd mod_wsgi
echo -e "开始配置NTP配置文件"
sed -i  -e '/server/d' -e "/fudge/d" /etc/ntp.conf
sed -i  -e "1i server 127.127.1.0" -e "2i fudge 127.127.1.0 stratum 10" /etc/ntp.conf
systemctl restart ntpd
systemctl enable ntpd
sed -i  "/^symbolic-links/a\default-storage-engine = innodb\ninnodb_file_per_table\ncollation-server = utf8_general_ci\ninit-connect = 'SET NAMES utf8'\ncharacter-set-server = utf8\nmax_connections=10000" /etc/my.cnf
systemctl daemon-reload
########################################################################################
crudini --set /etc/keystone/keystone.conf database connection  mysql+pymysql://keystone:$ALL_PASS@$HOST_NAME/keystone
ADMIN_TOKEN=$(openssl rand -hex 10)
crudini --set /etc/keystone/keystone.conf DEFAULT admin_token $ADMIN_TOKEN
crudini --set /etc/keystone/keystone.conf token provider  fernet
su -s /bin/sh -c "keystone-manage db_sync" keystone
keystone-manage fernet_setup --keystone-user keystone --keystone-group keystone
sed -i "s/#ServerName www.example.com:80/ServerName $HOST_NAME/g" /etc/httpd/conf/httpd.conf 
cat >/etc/httpd/conf.d/wsgi-keystone.conf<<- EOF
Listen 5000
Listen 35357

<VirtualHost *:5000>
    WSGIDaemonProcess keystone-public processes=5 threads=1 user=keystone group=keystone display-name=%{GROUP}
    WSGIProcessGroup keystone-public
    WSGIScriptAlias / /usr/bin/keystone-wsgi-public
    WSGIApplicationGroup %{GLOBAL}
    WSGIPassAuthorization On
    ErrorLogFormat "%{cu}t %M"
    ErrorLog /var/log/httpd/keystone-error.log
    CustomLog /var/log/httpd/keystone-access.log combined

    <Directory /usr/bin>
        Require all granted
    </Directory>
</VirtualHost>

<VirtualHost *:35357>
    WSGIDaemonProcess keystone-admin processes=5 threads=1 user=keystone group=keystone display-name=%{GROUP}
    WSGIProcessGroup keystone-admin
    WSGIScriptAlias / /usr/bin/keystone-wsgi-admin
    WSGIApplicationGroup %{GLOBAL}
    WSGIPassAuthorization On
    ErrorLogFormat "%{cu}t %M"
    ErrorLog /var/log/httpd/keystone-error.log
    CustomLog /var/log/httpd/keystone-access.log combined

    <Directory /usr/bin>
        Require all granted
    </Directory>
</VirtualHost>

EOF

systemctl enable httpd.service
systemctl start httpd.service

export OS_TOKEN=$ADMIN_TOKEN
export OS_URL=http://$HOST_NAME:35357/v3
export OS_IDENTITY_API_VERSION=3

openstack service create --name keystone --description "OpenStack Identity" identity
openstack endpoint create --region RegionOne identity public http://$HOST_NAME:5000/v3 
openstack endpoint create --region RegionOne identity internal http://$HOST_NAME:5000/v3
openstack endpoint create --region RegionOne identity admin http://$HOST_NAME:35357/v3

openstack domain create --description "Default Domain" $DOMAIN_NAME
openstack project create --domain $DOMAIN_NAME --description "Admin Project" admin
openstack user create --domain $DOMAIN_NAME --password $ALL_PASS admin

openstack role create admin
openstack role add --project admin --user admin admin

openstack project create --domain $DOMAIN_NAME --description "Service Project" service
openstack project create --domain $DOMAIN_NAME --description "Demo Project" demo

openstack user create --domain $DOMAIN_NAME --password $DEMO_PASS demo
openstack role create user
openstack role add --project demo --user demo user

unset OS_TOKEN OS_URL

cat > /etc/keystone/admin-openrc.sh <<-EOF
export OS_PROJECT_DOMAIN_NAME=$DOMAIN_NAME
export OS_USER_DOMAIN_NAME=$DOMAIN_NAME
export OS_PROJECT_NAME=admin
export OS_USERNAME=admin
export OS_PASSWORD=$ALL_PASS
export OS_AUTH_URL=http://$HOST_NAME:35357/v3
export OS_IDENTITY_API_VERSION=3
export OS_IMAGE_API_VERSION=2
EOF



cat > /etc/keystone/demo-openrc.sh <<-EOF
export OS_PROJECT_DOMAIN_NAME=$DOMAIN_NAME
export OS_USER_DOMAIN_NAME=$DOMAIN_NAME
export OS_PROJECT_NAME=demo
export OS_USERNAME=demo
export OS_PASSWORD=$DEMO_PASS
export OS_AUTH_URL=http://$HOST_NAME:35357/v3
export OS_IDENTITY_API_VERSION=3
export OS_IMAGE_API_VERSION=2
EOF
